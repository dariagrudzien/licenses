//A function to check if files from pull request include package.json

var https = require('https');
var env = require('./set.js');

//prepare the string for authentication
var auth = '&client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';

//prepare the path leading to end point (this should be updated dynamically, once the number of pull request is known)

var path = '/repos'+env.org+'dredd'+'/pulls/'+509+'/files';


checkPullFiles = function(host, path){

    var files = [];
    //prepare data necessary to connect to the API
    var options = {
        host: host,
        protocol: 'https:',
        path: path,
        headers: {
            'Accept': 'application/vnd.github.drax-preview+json',
            'user-agent': 'Agent'
        }
    };

    //connect to the API over https
    https.get(
        options, (res) => {

            let rawData = '';

            //grab data coming in the response
            res.on('data', (d) => {
                rawData += d;
            });

            //handle data once the whole response arrived
            res.on('end', () => {
                try {
                    const parsedData = JSON.parse(rawData);

                    parsedData.forEach(function (e) {
                        if(
                            //check if the string contains 'package.json'
                        )
                            //add the file into the array
                            { files.push(e['filename'])
                        
                    }, this);  

                    //log the array with the file
                    console.log(files);


                } catch (e) {
                    console.error(e.message);
                }
            })

        }).on('error', (e) => {
            console.error(e);
        });

}


// checkPullFiles(host, path);