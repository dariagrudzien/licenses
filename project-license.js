//Checks the license file inside of the repository

// Require config, https & npm api
var https = require('https');
var env = require('./set.js');
var api = require('api-npm');
// connect to GitHub api with auth
var auth = '?client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';



projectLicense = function(project){

    //prepare data necessary to connect to the API

    var options = {
        host: host,
        protocol: 'https:',
        path: '/repos'+env.org+project+'/license'+auth,
        headers: {
            'Accept': 'application/vnd.github.drax-preview+json',
            'user-agent': 'Agent'
        }
    };


    https.get(
        options, (res) => {

            let rawData = '';
            res.on('data', (d) => {
                rawData += d;
            });
            res.on('end', () => {
                try {
                    var project = JSON.parse(rawData);
                        //pushes into array all projects which don't have a license file, or the license is not MIT or BSD

                        if
                        (
                            (project['license'] == null)
                            ||
                            (
                                (project['license'] != null)
                                &&

                                (project['license']['name'].includes('MIT') == false)
                                &&
                                (project['license']['name'].includes('BSD') == false)

                            )
                        ) 
                        { console.log(project['name']) }
                        else 
                        { console.log(project['name']+' OK') }
                    
                } catch (e) {
                    console.error(e.message);
                }
            })

        }).on('error', (e) => {
            console.error(e);
        });
}
