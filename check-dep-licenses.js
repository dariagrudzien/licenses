//checking licenses of dependencies found in package.json files

var api = require('api-npm');

//an example of names of dependencies to test
var deps = [ 'async',
  'caseless',
  'chai',
  'clone',
  'coffee-script',
  'colors',
  'cross-spawn',
  'dredd-transactions',
  'file',
  'gavel',
  'glob',
  'html',
  'htmlencode',
  'inquirer',
  'js-yaml',
  'markdown-it',
  'optimist',
  'pitboss-ng',
  'proxyquire',
  'request',
  'spawn-args',
  'uuid',
  'which',
  'winston' ];

//checks name of the license for every dependency of a given project
deps.forEach(function (e){
    api.getdetails(e,test);

function test(data)
{
    //if the license detail is provided
    if(data['license'] != null){
        //log the name of the license
    console.log(data['license']);
} else {
    //otherwise log the name of the dependency with notification of violation
        console.log(e+' : missing license');
    }
}

});