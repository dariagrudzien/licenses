//A function to check how many pages with listed projects are there in a repository

// Require config, https & npm api
var https = require('https');
var env = require('./set.js');
var api = require('api-npm');
// connect to GitHub api with auth
var auth = '&client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';


function flip(page) {

    //prepare a variable which will store the number of pages listing projects within an organization
    var endpage = null;

    //prepare data necessary to connect to the API

    var options = {
        host: host,
        protocol: 'https:',
        path: '/orgs'+env.org+'repos?per_page=100&page='+page+auth,
        headers: {
            'Accept': 'application/vnd.github.drax-preview+json',
            'user-agent': 'Agent'
        }
    };


    https.get(
        options, (res) => {

            let rawData = '';
            res.on('data', (d) => {
                rawData += d;
            });

            res.on('end', () => {
                try {
                    //grab content-length from the response headers
                    var content = res.headers['content-length'];

                    //check if content-length is longer than 2 characters (more than an empty array)
                    if (content>2) {
                        //repeat the process for the next page
                        flip(page+1);
                    }
                    else {
                    
                        //save the total number of pages listing projects in an organization
                        endpage = page-1;
                        console.log(endpage);
                    
                }
                } catch (e) {
                    console.error(e.message);
                }
            })

        }).on('error', (e) => {
            console.error(e);
        });

}