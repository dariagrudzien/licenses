# LICENSE CHECKER v.0.1 - Work in Progress

## This application allows checking licenses of projects within a certain GitHub organization.

### First version will allow checking whether projects use MIT or BSD license. In case of Node.js projects with package.json files, it will also allow checking first-level dependencies to see if they violate the licensing requirements.

**Current state**: The app is broken down into small, independent parts dedicated to specific functionalities. Most of the parts are semi-finished. In the next step all parts will be combined in __app.js__ file, and a coherent logic will be added to ensure the app runs correctly and gathers all necessary information.