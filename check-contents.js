//A function to get a list of pull requests for a specific repo

var https = require('https');
var env = require('./set.js');



//prepare the string for authentication
var auth = '&client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';
//prepare the path leading to end point (this should be updated dynamically, once the total number of pages is known)
var path = '/repos'+env.org+'api-blueprint'+'/contents';


checkContents = function(host, path){

    //an array to store all files from a repo
    var files = [];
    //prepare data necessary to connect to the API
    var options = {
        host: host,
        protocol: 'https:',
        path: path,
        headers: {
            'Accept': 'application/vnd.github.drax-preview+json',
            'user-agent': 'Agent'
        }
    };

    //connect to the API over https
    https.get(
        options, (res) => {

            let rawData = '';

            //grab data coming in the response
            res.on('data', (d) => {
                rawData += d;
            });

            //handle data once the whole response arrived
            res.on('end', () => {
                try {
                    const parsedData = JSON.parse(rawData);

                    parsedData.forEach(function (e) {
                        //if there is a package.json file in the contents of the repository,    
                        
                        if(e['name'] == 'package.json')
                        files.push({
                            name: e['name']
                        });
                    }, this);  

                    if(files != false)
                    {console.log(files)}
                    else
                    {console.log('no package')}


                } catch (e) {
                    console.error(e.message);
                }
            })

        }).on('error', (e) => {
            console.error(e);
        });

}


// checkPulls(host, path);