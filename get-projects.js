//A function to prepare a list of all projects on a specific page in the organization

var https = require('https');
var env = require('./set.js');

//prepare the string for authentication
var auth = '&client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';
//prepare the path leading to end point (this should be updated dynamically, once the total number of pages is known)
var orgpath = '/orgs'+env.org+'repos?per_page=100&page='+1+auth;

// connect to GitHub api with auth

exports.getProjects = function(host, path, callback){

    //prepare an array into which all project names and links will be added
    var projects = [];

    //prepare data necessary to connect to the API
    var options = {
        host: host,
        protocol: 'https:',
        path: path,
        headers: {
            'Accept': 'application/vnd.github.drax-preview+json',
            'user-agent': 'Agent'
        }
    };

    //connect to the API over https
    https.get(
        options, (res) => {

            let rawData = '';

            //grab data coming in the response
            res.on('data', (d) => {
                rawData += d;
            });

            //handle data once the whole response arrived
            res.on('end', () => {
                try {
                    const parsedData = JSON.parse(rawData);

                    //add the name of the each project and the link into the array
                    parsedData.forEach(function (e) {
                 
                        projects.push({
                            name: e['name'],
                            url: e['html_url']
                        });
                    }, this);  

                    console.log(projects);
                    //return a list of projects in an array
                    return callback(projects);

                } catch (e) {
                    console.error(e.message);
                }
            })

        }).on('error', (e) => {
            console.error(e);
        });

}
