//requiring necessary modules
var https = require('https');
var api = require('api-npm');
var env = require('./set');
var projects = require('./get-projects');
var flip = require('./flip-pages');
var license = require('./project-license');
var contents = require('./check-contents');
var dependencies = require('./get-dependencies');
var deplicense = require('./check-dep-licenses');
var pulls = require('./check-pulls');
var pullfiles = require('./check-pull-files');



//preparing data which is being used throughout the app
var auth = '&client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';
var orgpath = '/orgs'+env.org+'repos?per_page=100&page='+page+auth;
// var repolicense = null;

//first page with repositories of the organization
var page = 1;
//needs to be updated by flipPage
var endpage = 3;
//all projects within the organization
var allprojects = [];
//projects which violate the requirements
var vprojects = [];


//prepare a list of all projects in the organization
projects.getProjects(host, orgpath, function(response){

    //logs the names and urls of the projects
    allprojects.push(response);
});



console.log(allprojects); 




// 1. Check how many pages of projects there are in the organization's repo, save that number into a variable
//      Use flip from flip-pages


// 2. Go through all pages and save names of all projects and links to their repos into an array
//      Use getProjects from get-projects

// 3. Check what licenses those projects have, save the results into an array either with the name of the license if it's not MIT/BSD or with 'ok' for reference, if everything is correct
//      Use projectLicense from project-license

// 4. Check if package.json exists
//      Use checkContents from check-contents

// 5. Decode contents of the package.json file and save the names of dependencies and devDependencies
//      Use getDependencies from get-dependencies

// 6. For all projects, check their dependencies, save the results into an array: if deps have MIT/BSD in the license name add 'ok' for reference, if they violate the requirements save the name of the license they use, if they don't have a license add a note about it
//      Use test from check-dep-licenses 

// 7. Print out two lists: first a list detailing all projects and dependencies within those projects which violate license requirements, then a list of names of projects which are okay, just for reference


// Checking pull request

// 8. Get a list of all pull requests for a specific project
//      Use checkPulls from check-pulls

// 9. Check files in the pull request to see if they include package.json

// 10. Check dependencies and devDependencies listed in that package.json
//      Use getDependencies from get-dependencies