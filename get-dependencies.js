//A function to get a list of all dependencies and devDependencies from package.json

var https = require('https');
var env = require('./set.js');
var atob = require('atob');


//prepare the string for authentication
var auth = '&client_id='+env.client+'&client_secret='+env.secret;
var host = 'api.github.com';
//prepare the path leading to end point (this should be updated dynamically, once the names of the projects are known)
var path = '/repos'+env.org+'dredd'+'/contents/package.json';


getDependencies = function(host, path){

    //prepare data necessary to connect to the API
    var options = {
        host: host,
        protocol: 'https:',
        path: path,
        headers: {
            'Accept': 'application/vnd.github.drax-preview+json',
            'user-agent': 'Agent'
        }
    };

    //connect to the API over https
    https.get(
        options, (res) => {

            let rawData = '';

            //grab data coming in the response
            res.on('data', (d) => {
                rawData += d;
            });

            //handle data once the whole response arrived
            res.on('end', () => {
                try {
                    const parsedData = JSON.parse(rawData);
                    var b64 = parsedData['content'];
                    //decode content
                    var bin = atob(b64);
                    var parsedContent = JSON.parse(bin);
                    //get all dependencies
                    var deps = parsedContent['dependencies'];
                    //get all devDependencies
                    var devdeps = parsedContent['devDependencies']
                    var keys = [];
                    var keysdev = [];

                    //add names of all dependencies into an array
                    for (var key in deps) {
                        if (deps.hasOwnProperty(key)) {
                        keys.push(key);
                        }
                    }
                    //add names of all devDependencies into an array
                    for (var key in devdeps) {
                        if (devdeps.hasOwnProperty(key)) {
                        keysdev.push(key);
                        }
                    }

                    console.log(keys);
                    console.log(keysdev);

                } catch (e) {
                    console.error(e.message);
                }
            })

        }).on('error', (e) => {
            console.error(e);
        });

}


// getDependencies(host, path);